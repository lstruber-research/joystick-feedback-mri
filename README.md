# Joystick Feedback MRI
Joystick Feedback MRI is a C++/Qt project to display feedback (real or visually distorded) from acquisition of joystick data. It is synchronized with MRI Scanner through TR impulsion, and all stimulation times are defined in function of TR.

## Installation
This project was coded on QtCreator 3.8.0 using Qt 4.8.6

## License
[MIT](https://choosealicense.com/licenses/mit/)
