/****************************************************************************
** Meta object code from reading C++ file 'feedbackthread.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../JoystickFeedback/feedbackthread.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'feedbackthread.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_FeedbackThread[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      13,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      10,       // signalCount

 // signals: signature, parameters, type, tag, flags
      33,   16,   15,   15, 0x05,
      54,   15,   15,   15, 0x05,
      70,   15,   15,   15, 0x05,
      85,   15,   15,   15, 0x05,
     102,   15,   15,   15, 0x05,
     113,   15,   15,   15, 0x05,
     124,   15,   15,   15, 0x05,
     141,   15,   15,   15, 0x05,
     151,   15,   15,   15, 0x05,
     175,  165,   15,   15, 0x05,

 // slots: signature, parameters, type, tag, flags
     195,   15,   15,   15, 0x0a,
     205,   15,   15,   15, 0x0a,
     221,   15,   15,   15, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_FeedbackThread[] = {
    "FeedbackThread\0\0target,deviation\0"
    "newTrial(int,double)\0startToCenter()\0"
    "backToCenter()\0doDisplayCross()\0"
    "endTrial()\0endBlock()\0interruptTrial()\0"
    "fbError()\0crossScreen()\0JoyX,JoyY\0"
    "dataSent(WORD,WORD)\0onError()\0"
    "onDataRequest()\0saveFinished()\0"
};

void FeedbackThread::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        FeedbackThread *_t = static_cast<FeedbackThread *>(_o);
        switch (_id) {
        case 0: _t->newTrial((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< double(*)>(_a[2]))); break;
        case 1: _t->startToCenter(); break;
        case 2: _t->backToCenter(); break;
        case 3: _t->doDisplayCross(); break;
        case 4: _t->endTrial(); break;
        case 5: _t->endBlock(); break;
        case 6: _t->interruptTrial(); break;
        case 7: _t->fbError(); break;
        case 8: _t->crossScreen(); break;
        case 9: _t->dataSent((*reinterpret_cast< WORD(*)>(_a[1])),(*reinterpret_cast< WORD(*)>(_a[2]))); break;
        case 10: _t->onError(); break;
        case 11: _t->onDataRequest(); break;
        case 12: _t->saveFinished(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData FeedbackThread::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject FeedbackThread::staticMetaObject = {
    { &QThread::staticMetaObject, qt_meta_stringdata_FeedbackThread,
      qt_meta_data_FeedbackThread, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &FeedbackThread::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *FeedbackThread::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *FeedbackThread::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_FeedbackThread))
        return static_cast<void*>(const_cast< FeedbackThread*>(this));
    return QThread::qt_metacast(_clname);
}

int FeedbackThread::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QThread::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 13)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 13;
    }
    return _id;
}

// SIGNAL 0
void FeedbackThread::newTrial(int _t1, double _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void FeedbackThread::startToCenter()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}

// SIGNAL 2
void FeedbackThread::backToCenter()
{
    QMetaObject::activate(this, &staticMetaObject, 2, 0);
}

// SIGNAL 3
void FeedbackThread::doDisplayCross()
{
    QMetaObject::activate(this, &staticMetaObject, 3, 0);
}

// SIGNAL 4
void FeedbackThread::endTrial()
{
    QMetaObject::activate(this, &staticMetaObject, 4, 0);
}

// SIGNAL 5
void FeedbackThread::endBlock()
{
    QMetaObject::activate(this, &staticMetaObject, 5, 0);
}

// SIGNAL 6
void FeedbackThread::interruptTrial()
{
    QMetaObject::activate(this, &staticMetaObject, 6, 0);
}

// SIGNAL 7
void FeedbackThread::fbError()
{
    QMetaObject::activate(this, &staticMetaObject, 7, 0);
}

// SIGNAL 8
void FeedbackThread::crossScreen()
{
    QMetaObject::activate(this, &staticMetaObject, 8, 0);
}

// SIGNAL 9
void FeedbackThread::dataSent(WORD _t1, WORD _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 9, _a);
}
QT_END_MOC_NAMESPACE
