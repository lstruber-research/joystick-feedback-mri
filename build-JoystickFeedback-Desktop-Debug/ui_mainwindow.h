/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QComboBox>
#include <QtGui/QGraphicsView>
#include <QtGui/QHeaderView>
#include <QtGui/QMainWindow>
#include <QtGui/QPushButton>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QGraphicsView *FeedbackView;
    QPushButton *StartButton;
    QPushButton *CloseButton;
    QPushButton *BackButton;
    QComboBox *comboBox;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(955, 666);
        MainWindow->setStyleSheet(QString::fromUtf8("QPushButton#StartButton{border-image:url(:/img/play);}\n"
"QPushButton#StartButton:hover{border-image:url(:/img/play_hoovered);}\n"
"QPushButton#StartButton:pressed{border-image:url(:/img/play_clicked);}\n"
"\n"
"QPushButton#CloseButton{border-image:url(:/img/close);}\n"
"QPushButton#CloseButton:hover{border-image:url(:/img/close_hoovered);}\n"
"QPushButton#CloseButton:pressed{border-image:url(:/img/close_clicked);}\n"
"\n"
"QPushButton#BackButton{border-image:url(:/img/back);}\n"
"QPushButton#BackButton:hover{border-image:url(:/img/back_hoovered);}\n"
"QPushButton#BackButton:pressed{border-image:url(:/img/back_clicked);}"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        FeedbackView = new QGraphicsView(centralWidget);
        FeedbackView->setObjectName(QString::fromUtf8("FeedbackView"));
        FeedbackView->setGeometry(QRect(0, 0, 256, 192));
        StartButton = new QPushButton(centralWidget);
        StartButton->setObjectName(QString::fromUtf8("StartButton"));
        StartButton->setGeometry(QRect(400, 330, 304, 300));
        CloseButton = new QPushButton(centralWidget);
        CloseButton->setObjectName(QString::fromUtf8("CloseButton"));
        CloseButton->setGeometry(QRect(843, 20, 30, 30));
        BackButton = new QPushButton(centralWidget);
        BackButton->setObjectName(QString::fromUtf8("BackButton"));
        BackButton->setEnabled(true);
        BackButton->setGeometry(QRect(890, 20, 39, 30));
        comboBox = new QComboBox(centralWidget);
        comboBox->setObjectName(QString::fromUtf8("comboBox"));
        comboBox->setGeometry(QRect(500, 250, 211, 41));
        QFont font;
        font.setPointSize(14);
        font.setBold(true);
        font.setWeight(75);
        comboBox->setFont(font);
        MainWindow->setCentralWidget(centralWidget);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0, QApplication::UnicodeUTF8));
        StartButton->setText(QString());
        CloseButton->setText(QString());
        BackButton->setText(QString());
        comboBox->clear();
        comboBox->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "Resting State", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("MainWindow", "Normal", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("MainWindow", "Random Rotation", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("MainWindow", "Rotation", 0, QApplication::UnicodeUTF8)
        );
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
