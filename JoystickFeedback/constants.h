#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <vector>
#include <string>
#include <ctime>
#include <fstream>

#endif // CONSTANTS_H

using namespace std;

// CONSTANTS ISSUED FROM FILE
extern int TARGET_NUMBER;
extern int TARGET_DISTANCE;
extern int TARGET_SIZE;
extern int CENTER_SIZE;
extern int BALL_SIZE;

extern int CONSTANT_DEV;
extern int RDM_MAX_DEV;
extern int RDM_STEP_DEV;

extern int REST_TR;
extern int TRIALS_TR;
extern int INIT_TR;
extern int NB_TRIALS;
extern int DISPLAY_TARGET_TIME;
extern int TIME_TO_VALID_TARGET;
extern int TR_IRM;
extern int RESTING_SCAN_TIME;

extern int DATA_RATE;

extern int NB_BLOCKS_NORMAL;
extern int NB_BLOCKS_ROT;
extern int NB_BLOCKS_RANDOM_ROT;

// BOARD CONFIGURATION
extern int BOARD_NUM;
extern int IRM_TRIG_CHANNEL;
extern int JOY_X_CHANNEL;
extern int JOY_Y_CHANNEL;
extern int LOW_CHANNEL;
extern int HIGH_CHANNEL;

// CONDITIONS
#define RESTING     0
#define NORMAL      1
#define RANDOM_ROT  2
#define ROT         3

extern std::vector<int> PossibleDevAngles;
extern std::vector<int> TargetList;
extern float JoyXCalib, JoyYCalib, JoyXAmp, JoyYAmp;
extern int trialTime;

// LOGGING FUNCTIONS
inline string getCurrentDateTime(string s){
    time_t now = time(0);
    struct tm  tstruct;
    char  buf[80];
    localtime_s(&tstruct, &now);

    if(s=="now")
        strftime(buf, sizeof(buf), "%Y-%m-%d %X", &tstruct);
    else if(s=="date")
        strftime(buf, sizeof(buf), "%Y-%m-%d", &tstruct);
    return string(buf);
}

inline void Logger(string logMsg, char* dataFolder){
    string strDataFolder(dataFolder);
    string filePath = strDataFolder + "\\LOG_" + getCurrentDateTime("date")+".log";
    string now = getCurrentDateTime("now");
    ofstream ofs(filePath.c_str(), std::ios_base::out | std::ios_base::app );
    ofs << now << '\t' << logMsg << '\n';
    ofs.close();
}
