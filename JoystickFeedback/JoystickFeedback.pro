#-------------------------------------------------
#
# Project created by QtCreator 2017-11-21T16:51:23
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = JoystickFeedback
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    feedback.cpp \
    feedbackthread.cpp \
    savingthread.cpp

HEADERS  += mainwindow.h \
    feedback.h \
    constants.h \
    feedbackthread.h \
    cbw.h \
    savingthread.h

FORMS    += mainwindow.ui

RESOURCES += \
    img.qrc

LIBS += -L"lib" -lcbw32

