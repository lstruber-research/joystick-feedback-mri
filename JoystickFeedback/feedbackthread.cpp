#include "feedbackthread.h"

using namespace std;

int DISP_RATE = 30;

FeedbackThread::FeedbackThread(int phase, int nbBlocks, char* dataFolder, QString username, int fileNumber) : iPhase(phase), iNbBlocks(nbBlocks)
{
    memcpy(mDataFolder, dataFolder, sizeof(mDataFolder));
    DevAngleVector.clear();
    TargetsVector.clear();
    stopRequest = false;

    char fileNumberChar[4] ;
    sprintf_s(fileNumberChar, "%d", fileNumber + 1);

    // Create file for data saving
    switch(iPhase)
    {
        case NORMAL:
        {
            strcpy_s(fileName, mDataFolder);
            strcat_s(fileName, "\\RAW_F_");
            strcat_s(fileName, fileNumberChar);
            strcat_s(fileName, "_");
            strcat_s(fileName, username.toStdString().c_str());
            break;
        }
        case ROT:
        {
            strcpy_s(fileName, mDataFolder);
            strcat_s(fileName, "\\RAW_R_");
            strcat_s(fileName, fileNumberChar);
            strcat_s(fileName, "_");
            strcat_s(fileName, username.toStdString().c_str());
            break;
        }
        case RANDOM_ROT:
        {
            strcpy_s(fileName, mDataFolder);
            strcat_s(fileName, "\\RAW_RR_");
            strcat_s(fileName, fileNumberChar);
            strcat_s(fileName, "_");
            strcat_s(fileName, username.toStdString().c_str());
            break;
        }
    }

    Rate = (long) DATA_RATE;
    iNbChannels = (HIGH_CHANNEL - LOW_CHANNEL + 1);
    // Count = (((double)(TR_IRM * (TRIALS_TR + REST_TR)))/1000 * Rate) * iNbChannels * nbBlocks;
    Count = ((long)(((double)(TR_IRM * (TRIALS_TR + INIT_TR)))/1000 * Rate)) * iNbChannels; // Seconds * Samples/sec * NbTrials * NbChannels
    BufferData = new WORD[Count];
    memset(BufferData,0,Count*sizeof(WORD));

    cout << "Starting Run..." << endl;
    cout << "NbChannels : " << iNbChannels << endl;
    cout << "NbTrials : " << NB_TRIALS << endl;
    cout << "Count : " << Count << endl;

    if(iPhase != RESTING) Logger("Initializing run (" + to_string(static_cast<long long>(iNbBlocks)) + " blocks of " + to_string(static_cast<long long>(NB_TRIALS)) + " trials or " + to_string(static_cast<long long>(Count)) + " samples per block on " + to_string(static_cast<long long>(iNbChannels)) + " channels)", mDataFolder);
    else Logger("Starting resting run", mDataFolder);

    trialNumber = new int[Count/iNbChannels];
    memset(trialNumber,-1,Count/iNbChannels*sizeof(int));
    kinTrigValue = new int[Count/iNbChannels];
    memset(kinTrigValue,0,Count/iNbChannels*sizeof(int));
    targetNumber = new int[Count/iNbChannels];
    memset(targetNumber,-1,Count/iNbChannels*sizeof(int));
    angleValue = new int[Count/iNbChannels];
    memset(angleValue,-1,Count/iNbChannels*sizeof(int));

    threadSave = NULL;
}

void FeedbackThread::run()
{
    if(iPhase == RESTING) {
        emit(crossScreen());

        Logger("Waiting for IRM trigger...", mDataFolder);
        if(!waitIRMTrig()) {
            cout << "waitIRMTrig Error" << endl;
            Logger("ERROR : error while waiting IRM trigger", mDataFolder);
            emit fbError();
        }
        Logger("Start run (rest)", mDataFolder);
        clock_t start_run = clock();
        cout << "Start Rest" << endl;
        double startTime = ((double) clock()/CLOCKS_PER_SEC)*1000;
        double currentTime = startTime;
        while(currentTime - startTime < RESTING_SCAN_TIME) {
              currentTime = ((double) clock()/CLOCKS_PER_SEC)*1000;
              this->msleep(100);
              if(stopRequest)
              {
                  Logger("Run interrupted by user", mDataFolder);
                  emit interruptTrial();
                  return;
              }
        }
        clock_t end_run = clock();
        int run_time = (int) ((double) (end_run - start_run)/CLOCKS_PER_SEC * 1000);
        Logger("Run finished in " + to_string(static_cast<long long>(run_time)) + "ms", mDataFolder);
    }
    else {
        srand(time(NULL));


        feedbackError = false;
        cout << "Buffer Size : " << Count << endl;
        emit doDisplayCross();

        Logger("Waiting for IRM trigger...", mDataFolder);
        if(!waitIRMTrig()) {
            cout << "waitIRMTrig Error" << endl;
            Logger("ERROR : error while waiting IRM trigger", mDataFolder);
            emit fbError();
        }
        Logger("Start run", mDataFolder);
        clock_t start_run = clock();
        clock_t end_rest = start_run;

        for(int iBlock = 0; iBlock < iNbBlocks; iBlock++)
        {

            cout << "Extract sequence" << endl;
            extractAngleSequence();
            cout << "Extracted" << endl;

            if(!launchRecording(Count,Rate,BufferData)) {
                Logger("ERROR : error while launching recording", mDataFolder);
                cout << "Launch recording Error" << endl;
                emit fbError();
                break;
            }
            else {
                Logger("Starting block " + to_string(static_cast<long long>(iBlock + 1)) + " over " + to_string(static_cast<long long>(iNbBlocks)), mDataFolder);
                emit startToCenter();

                clock_t start_block = clock();
                int startingTime = (int) ((double) (start_block - end_rest)/CLOCKS_PER_SEC * 1000);

                this->msleep(INIT_TR*TR_IRM - startingTime);

                start_block = clock();
                startingTime = (int) ((double) (start_block - end_rest)/CLOCKS_PER_SEC * 1000);
                cout << "starting time : " << startingTime << endl;
                Logger("Block initialization time " + to_string(static_cast<long long>(startingTime)) + "ms", mDataFolder);

                for(int iTrial = 0; iTrial < NB_TRIALS; iTrial++)
                {
                    long StartTrialIdx = getCurrentIdx();
                    if(StartTrialIdx == -1) {
                        emit fbError();
                        break;
                    }
                    setKinematicTrigger((int) 1, StartTrialIdx);
                    setTrial((int)iTrial, StartTrialIdx);
                    setTargetNumber((int)TargetsVector[iTrial], StartTrialIdx);
                    setAngle((int)floor(DevAngleVector[iTrial]*180/3.1415 + 0.5), StartTrialIdx);

                    targetReached = false;
                    // Draw targets in QPainter
                    /*cout << "Start trial number " << iTrial + 1 << endl;
                    cout << "TargetsVector[iTrial] " << TargetsVector[iTrial] << endl;
                    cout << "DevAngleVector[iTrial] " << DevAngleVector[iTrial] << endl;*/
                    emit newTrial(TargetsVector[iTrial], DevAngleVector[iTrial]);
                    //cout << "DISPLAY_TARGET_TIME " << DISPLAY_TARGET_TIME << endl;

                    this->msleep(DISPLAY_TARGET_TIME);

                    long BackCenterIdx = getCurrentIdx();
                    if(BackCenterIdx == -1) {
                        emit fbError();
                        break;
                    }
                    setKinematicTrigger(2, BackCenterIdx);

                    emit backToCenter();

                    this->msleep(trialTime - DISPLAY_TARGET_TIME - 1);

                    if(feedbackError)
                    {
                        cout << "Feedback Error : " << feedbackError << endl;
                        emit fbError();
                        break;
                    }
                    if(stopRequest){
                        Logger("Run interrupted by user", mDataFolder);
                        break;
                    }
                    emit endTrial();

                }
                emit endBlock(); // boolean 0 = timeout, 1 = target reached
                emit doDisplayCross();

                Logger("End of block, stopping record", mDataFolder);
                stopRecording(iBlock);
                if(stopRequest) break;

                clock_t start_rest = clock();
                int block_time = (int) ((double) (start_rest - start_block)/CLOCKS_PER_SEC * 1000);
                cout << "block time : " << block_time << endl;
                Logger("Block finished in " + to_string(static_cast<long long>(block_time)) + "ms", mDataFolder);

                memset(BufferData,0,Count*sizeof(WORD));
                memset(trialNumber,-1,Count/iNbChannels*sizeof(int));
                memset(kinTrigValue,0,Count/iNbChannels*sizeof(int));
                memset(targetNumber,-1,Count/iNbChannels*sizeof(int));
                memset(angleValue,-1,Count/iNbChannels*sizeof(int));

                this->msleep(REST_TR*TR_IRM - TR_IRM/2);
                if(!waitIRMTrig()) {
                    Logger("ERROR : error while waiting IRM trigger", mDataFolder);
                    cout << "waitIRMTrig Error" << endl;
                    emit fbError();
                }
                end_rest = clock();
                int rest_time = (int) ((double) (end_rest - start_rest)/CLOCKS_PER_SEC * 1000);
                cout << "rest time : " << rest_time << endl;
                Logger("Rest finished in " + to_string(static_cast<long long>(rest_time)) + "ms", mDataFolder);
            }
        }
        clock_t end_run = clock();
        int run_time = (int) ((double) (end_run - start_run)/CLOCKS_PER_SEC * 1000);
        Logger("Run finished in " + to_string(static_cast<long long>(run_time)) + "ms", mDataFolder);
    }
}

void FeedbackThread::interrupt()
{
    stopRequest = true;
}

void FeedbackThread::onError()
{
    feedbackError = true;
}

void FeedbackThread::onDataRequest() {
    short Status = 0;
    long CurCount, CurIndex;
    int ULStat = cbGetStatus(BOARD_NUM, &Status, &CurCount, &CurIndex, AIFUNCTION);
    // cout << "CurIdx : " << CurIndex << " / CurCount : " << CurCount << endl;
    if(ULStat != 0) {
        cout << "cbGetStatus Error : " << ULStat << endl;
        Logger("ERROR : cbGetStatus failed", mDataFolder);
        feedbackError = true;
        return;
    }
    else {
        int meanJoyX = 0;
        int meanJoyY = 0;
        for(int i = 0; i < DATA_RATE/DISP_RATE; i++) {
            meanJoyX += (int) BufferData[CurIndex + 1 - 3*i];
            meanJoyY += (int) BufferData[CurIndex - 3*i];
        }
        meanJoyX /= DATA_RATE/DISP_RATE;
        meanJoyY /= DATA_RATE/DISP_RATE;
        //int meanJoyX = BufferData[CurIndex + 1];
        //int meanJoyY = BufferData[CurIndex];
        emit dataSent((WORD) meanJoyX, (WORD) meanJoyY);
    }
}

long FeedbackThread::getCurrentIdx() {
    short Status = 0;
    long CurCount, CurIndex;
    int ULStat = cbGetStatus(BOARD_NUM, &Status, &CurCount, &CurIndex, AIFUNCTION);
    //cout << "getstatusok" << endl;
    if(ULStat != 0) {
        cout << "cbGetStatus Error : " << ULStat << endl;
        Logger("ERROR : cbGetStatus failed", mDataFolder);
        feedbackError = true;
        return -1;
    }
    else return CurIndex/iNbChannels;
}

bool FeedbackThread::waitIRMTrig() {
    WORD DataValue = 0;
    int ULStat = cbATrig(BOARD_NUM, IRM_TRIG_CHANNEL, TRIGABOVE, 40000, BIP5VOLTS, &DataValue); // TODO : replace 10000 by 40000
    if(ULStat != 0)
    {
        cout << "cbFileAInScan Error : " << ULStat << endl;
        return false;
    }
    return true;
}

bool FeedbackThread::launchRecording(long Count, long Rate, WORD* DataBuffer) {
    unsigned Options = NOCONVERTDATA + BACKGROUND + SINGLEIO;
    int ULStat = cbAInScan(BOARD_NUM, LOW_CHANNEL, HIGH_CHANNEL, Count, &Rate, BIP5VOLTS, DataBuffer, Options);
    if(ULStat != 0)
    {
        cout << "cbFileAInScan Error : " << ULStat << endl;
        return false;
    }
    return true;
}

void FeedbackThread::setTrial(int value, long idx) {
    trialNumber[idx] = value;
}

void FeedbackThread::setKinematicTrigger(int value, long idx) {
    kinTrigValue[idx] = value;
}

void FeedbackThread::setTargetNumber(int value, long idx) {
    targetNumber[idx] = value;
}

void FeedbackThread::setAngle(int value, long idx) {
   angleValue[idx] = value;
}

bool FeedbackThread::stopRecording(int numBlock) {
    int ULStat = cbStopBackground(BOARD_NUM, AIFUNCTION);
    if(ULStat != 0)
    {
        cout << "cbStopBackground Error : " << ULStat << endl;
        return false;
    }

    if(!stopRequest) {
        // SAVING ALL DATA
        if(threadSave == NULL) {
            threadSave = new SavingThread(fileName,numBlock,Count,iNbChannels, BufferData, trialNumber, kinTrigValue, targetNumber, angleValue);
            connect(threadSave, SIGNAL(finished()), this, SLOT(saveFinished()));
            threadSave->start();
        }
        else
            cout << "Error : Saving already in progress" << endl;
    }
    return true;
}

void FeedbackThread::saveFinished() {
    cout << "File Saved" << endl;
    delete threadSave;
    threadSave = NULL;
}

FeedbackThread::~FeedbackThread()
{
    if(threadSave != NULL) {threadSave->wait(); this->msleep(200);}
    if(BufferData) delete[] BufferData;
    if(trialNumber) delete[] trialNumber;
    if(kinTrigValue) delete[] kinTrigValue;
    if(targetNumber) delete[] targetNumber;
    if(angleValue) delete[] angleValue;
}

void FeedbackThread::extractAngleSequence()
{
    switch(iPhase) {
        case NORMAL:
            DevAngleVector.resize(NB_TRIALS,0);
            TargetsVector.resize(NB_TRIALS);
            for(int i = 0; i < NB_TRIALS; i++)
                TargetsVector[i] = rand() % TARGET_NUMBER;
            break;
        case ROT:
            DevAngleVector.resize(NB_TRIALS,(double) CONSTANT_DEV * PI/180);
            TargetsVector.resize(NB_TRIALS);
            for(int i = 0; i < NB_TRIALS; i++)
                TargetsVector[i] = rand() % TARGET_NUMBER;
            break;
        case RANDOM_ROT:
            vector<double> IndexVector;
            vector<double> OrderedDevAngleVector;
            vector<double> OrderedTargetsVector;
            OrderedDevAngleVector.resize(NB_TRIALS);
            OrderedTargetsVector.resize(NB_TRIALS);
            IndexVector.resize(NB_TRIALS);
            int NbAngles = PossibleDevAngles.size();

            // MOINS D'ESSAIS QUE D'ANGLES POSSIBLES
            if(NB_TRIALS < NbAngles) {
                for(int i = NB_TRIALS; i < NB_TRIALS; i++)
                {
                    int randIdx = rand()% NbAngles;
                    OrderedDevAngleVector[i] = PossibleDevAngles[randIdx];
                    OrderedTargetsVector[i] = rand() % TARGET_NUMBER;
                    IndexVector[i] = i;
                }
            }
            if(NB_TRIALS < TARGET_NUMBER*NbAngles) {
                int N = (int)(((double)NB_TRIALS)/((double)NbAngles));
                for(int i = 0; i < N; i++)
                {
                    for(int j = 0; j < NbAngles; j++)
                    {
                        OrderedDevAngleVector[j + i*NbAngles] = PossibleDevAngles[j];
                        OrderedTargetsVector[j + i*NbAngles] = rand() % TARGET_NUMBER;
                        IndexVector[j + i*NbAngles] = j + i*NbAngles;
                    }
                }

                int lastTrials = NB_TRIALS - N*NbAngles;
                for(int i = NB_TRIALS - lastTrials; i < NB_TRIALS; i++)
                {
                    OrderedDevAngleVector[i] = 0;
                    OrderedTargetsVector[i] = rand() % TARGET_NUMBER;
                    IndexVector[i] = i;
                }
            }
            else {

                for(int i = 0; i < TARGET_NUMBER; i++)
                {
                    for(int j = 0; j < NbAngles; j++)
                    {
                        OrderedDevAngleVector[j + i*NbAngles] = PossibleDevAngles[j];
                        OrderedTargetsVector[j + i*NbAngles] = i;
                        IndexVector[j + i*NbAngles] = j + i*NbAngles;
                    }
                }

                int N = (int)(((double)NB_TRIALS)/((double)NbAngles) - TARGET_NUMBER);
                for(int i = 0; i < N; i++)
                {
                    for(int j = 0; j < NbAngles; j++)
                    {
                        OrderedDevAngleVector[j + (TARGET_NUMBER + i)*NbAngles] = PossibleDevAngles[j];
                        OrderedTargetsVector[j + (TARGET_NUMBER + i)*NbAngles] = rand() % TARGET_NUMBER;
                        IndexVector[j + (TARGET_NUMBER + i)*NbAngles] = j + (TARGET_NUMBER + i)*NbAngles;
                    }
                }

                int lastTrials = NB_TRIALS - (TARGET_NUMBER + N)*NbAngles;
                for(int i = NB_TRIALS - lastTrials; i < NB_TRIALS; i++)
                {
                    OrderedDevAngleVector[i] = 0;
                    OrderedTargetsVector[i] = rand() % TARGET_NUMBER;
                    IndexVector[i] = i;
                }
            }

            std::random_shuffle(IndexVector.begin(), IndexVector.end());

            DevAngleVector.resize(NB_TRIALS);
            TargetsVector.resize(NB_TRIALS);

            for(int k = 0; k < NB_TRIALS; k++)
            {
                DevAngleVector[k] = OrderedDevAngleVector[IndexVector[k]]*PI/180;
                TargetsVector[k] = OrderedTargetsVector[IndexVector[k]];
            }
            break;
    }
}
