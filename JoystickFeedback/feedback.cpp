#include "feedback.h"

using namespace std;

int TARGET_NUMBER;
int TARGET_DISTANCE;
int TARGET_SIZE;
int CENTER_SIZE;
int BALL_SIZE;

int CONSTANT_DEV;
int RDM_MAX_DEV;
int RDM_STEP_DEV;

int REST_TR;
int TRIALS_TR;
int INIT_TR;
int NB_TRIALS;
int DISPLAY_TARGET_TIME;
int TIME_TO_VALID_TARGET;
int TR_IRM;
int RESTING_SCAN_TIME;

int NB_BLOCKS_NORMAL;
int NB_BLOCKS_ROT;
int NB_BLOCKS_RANDOM_ROT;

int DATA_RATE;
int DISPLAY_RATE = 30;

int BOARD_NUM;
int IRM_TRIG_CHANNEL;
int JOY_X_CHANNEL;
int JOY_Y_CHANNEL;
int LOW_CHANNEL;
int HIGH_CHANNEL;

vector<int> PossibleDevAngles;
vector<int> TargetList;
float JoyXCalib, JoyYCalib;
float JoyXAmp, JoyYAmp;
int trialTime;

Feedback::Feedback(QGraphicsView &view, int phase, int trigRadius) : iView(view), iPhase(phase), iTrigRadius(trigRadius)
{
    iTargetNum = 0;
    dAngle = 0;
    dRealAngularDev = 0;
    eventLaunched = false;

    startTargetXCenter = 9999;
    startTargetYCenter = 9999;
    realTargetXCenter = 9999;
    realTargetYCenter = 9999;
    bTargetToCenter = true;

    iScene = new QGraphicsScene((QObject*)&iView);
    iScene->setBackgroundBrush(QBrush(QColor(125,125,125)));
    updateDisplayTimer = new QTimer(this);
    updateDisplayTimer->setInterval((int) 1000/DISPLAY_RATE);
    connect(updateDisplayTimer, SIGNAL(timeout()), this, SLOT(dataRequestForDisplayUpdate()));

    fError = false;
    initCanceled = false;

    iTargets.resize(TARGET_NUMBER);

    iScene->clear();

    for(int i = 0; i < TARGET_NUMBER; i++)
    {
        int targetX = TARGET_DISTANCE*cos(i*2*PI/TARGET_NUMBER);
        int targetY = TARGET_DISTANCE*sin(i*2*PI/TARGET_NUMBER);
        iTargets[i] = new QGraphicsEllipseItem(- TARGET_SIZE/2, - TARGET_SIZE/2,TARGET_SIZE,TARGET_SIZE);
        iTargets[i]->setPos(targetX, targetY);
        iTargets[i]->setBrush(QBrush(Qt::transparent));
        iTargets[i]->setPen(QPen(Qt::transparent));
        iScene->addItem(iTargets[i]);
    }

    iBall = new QGraphicsEllipseItem(-BALL_SIZE/2, -BALL_SIZE/2, BALL_SIZE, BALL_SIZE);
    iBall->setBrush(QBrush(QColor(0,128,0)));
    iBall->setPen(QPen(Qt::transparent));

    iCenter = new QGraphicsEllipseItem(-CENTER_SIZE/2, -CENTER_SIZE/2, CENTER_SIZE, CENTER_SIZE);
    iCenter->setBrush(QBrush(Qt::transparent));
    iCenter->setPen(QPen(Qt::transparent));

    iCrossHor = new QGraphicsLineItem(-25,0,25,0);
    iCrossHor->setPen(QPen(Qt::transparent));
    iCrossVert = new QGraphicsLineItem(0,-25,0,25);
    iCrossVert->setPen(QPen(Qt::transparent));

    iScene->addItem(iCrossHor);
    iScene->addItem(iCrossVert);
    iScene->addItem(iCenter);
    iScene->addItem(iBall);

    iView.setScene(iScene);
}

bool Feedback::isInitCanceled()
{
    return initCanceled;
}

void Feedback::startNewTrial(int TargetNum, double Angle)
{
    /*iTargets[iTargetNum]->setBrush(QBrush(Qt::transparent));
    iTargets[iTargetNum]->setPen(QPen(QColor(160,0,0)));*/
    bTargetToCenter = false;

    iTargetNum = TargetNum;
    dAngle = Angle;
    dRealAngularDev = Angle;
    //cout << Angle << endl;
    eventLaunched = false;

    if(iTargetNum >= TARGET_NUMBER)
        iTargetNum = TARGET_NUMBER - 1;

    startTargetXCenter = TARGET_DISTANCE*cos(iTargetNum*2*PI/TARGET_NUMBER);
    startTargetYCenter = TARGET_DISTANCE*sin(iTargetNum*2*PI/TARGET_NUMBER);
    realTargetXCenter = startTargetXCenter;
    realTargetYCenter = startTargetYCenter;

    iTargets[iTargetNum]->setBrush(QBrush(QColor(160,0,0)));
    iTargets[iTargetNum]->setPen(QPen(QColor(160,0,0)));

    iCenter->setBrush(QBrush(Qt::transparent));
    iCenter->setPen(QPen(Qt::transparent));  
}

void Feedback::initToCenter()
{
    updateDisplayTimer->start();

    BallXCenter_old = 0;
    BallYCenter_old = 0;
    JoystickX_old = 0;
    JoystickY_old = 0;

    targetTimes = 0;

    iBall->setBrush(QBrush(QColor(0,128,0)));

    for(int i = 0; i < TARGET_NUMBER; i++) {
        iTargets[i]->setPen(QPen(QColor(160,0,0),10));
        iTargets[i]->setBrush(QBrush(Qt::transparent));
    }

    iCenter->setBrush(QBrush(QColor(160,0,0)));
    iCrossHor->setPen(QPen(QColor(125,125,125),5));
    iCrossVert->setPen(QPen(QColor(125,125,125),5));
}

void Feedback::setTargetToCenter()
{
    bTargetToCenter = true;
    iCenter->setBrush(QBrush(QColor(160,0,0)));
    iTargets[iTargetNum]->setPen(QPen(QColor(160,0,0),10));
    iTargets[iTargetNum]->setBrush(QBrush(Qt::transparent));
}

void Feedback::displayCross()
{
    iBall->setBrush(QBrush(Qt::transparent));
    for(int i = 0; i < TARGET_NUMBER; i++) {
        iTargets[i]->setPen(QPen(Qt::transparent));
        iTargets[i]->setBrush(QBrush(Qt::transparent));
    }
    iCrossHor->setPen(QPen(QColor(20,20,20),5));
    iCrossVert->setPen(QPen(QColor(20,20,20),5));

    iCenter->setBrush(QBrush(Qt::transparent));
}

void Feedback::setBallPos(float x, float y){
    //cout << x << ";" << y << endl;
    iBall->setPos(x,y);
    iBall->setBrush(QBrush(QColor(0,128,0)));
    /* QGraphicsLineItem *traj = iScene->addLine(BallXCenter_old,BallYCenter_old,x,y);
    traj->setPen(QPen(QColor(0,128,0),5));*/
}

void Feedback::startEvent() {
    if(iPhase == 9999) // FOR JUMP
    {        
        double newTargetAngle = iTargetNum*2*PI/TARGET_NUMBER + dAngle;

        realTargetXCenter = TARGET_DISTANCE*cos(newTargetAngle);
        realTargetYCenter = TARGET_DISTANCE*sin(newTargetAngle);
        iTargets[iTargetNum]->setPos(realTargetXCenter,realTargetYCenter);
        /*cout << "Deviation Angle : " << 180/PI*dAngle << endl;
        cout << "Target Angle : " << 180/PI*iTargetNum*2*PI/TARGET_NUMBER << endl;
        cout << "New Target Angle : " << 180/PI*newTargetAngle << endl;*/
    }
    else if(iPhase == ROT || iPhase == RANDOM_ROT)
    {
        dRealAngularDev = dAngle;
        cout << "Deviation Angle : " << 180/PI*dAngle << endl;
    }
}

void Feedback::dataRequestForDisplayUpdate() {
    emit dataRequest();
}

void Feedback::onDataReceived(WORD JoyX, WORD JoyY)
{
    float XVolt = ((float)JoyX) * 10/65535 - 5;
    float YVolt = ((float)JoyY) * 10/65535 - 5;

    // cout << XVolt << ";" << YVolt << endl;

    // Asservissement position
    float JoystickX = -((((float)TARGET_DISTANCE) + 100)/JoyXAmp * (XVolt - JoyXCalib));
    float JoystickY = -((((float)TARGET_DISTANCE) + 100)/JoyYAmp * (YVolt - JoyYCalib));

    float BallXCenter, BallYCenter;

    float DecX = JoystickX - JoystickX_old;
    float DecY = JoystickY - JoystickY_old;

    BallXCenter = BallXCenter_old + DecX * cos(dRealAngularDev) + DecY * sin(dRealAngularDev);
    BallYCenter = BallYCenter_old + DecY * cos(dRealAngularDev) - DecX * sin(dRealAngularDev);

    JoystickX_old = JoystickX;
    JoystickY_old = JoystickY;


    setBallPos(BallXCenter,BallYCenter);

    /*if(!eventLaunched && (sqrt(pow(BallXCenter,2) + pow(BallYCenter,2)) > iTrigRadius)) {
        eventLaunched = true;
        startEvent();
    }*/

    // iTargets[iTargetNum]->setBrush(QBrush(QColor(160,0,0)));

    int dist = BalltoTargetDist(BallXCenter, BallYCenter);
    BallXCenter_old = BallXCenter;
    BallYCenter_old = BallYCenter;

    if((dist < TARGET_SIZE/2) && (bTargetToCenter == false))
    {
        targetTimes++;
        double targetTime = 1000/DISPLAY_RATE * targetTimes;
        if(targetTime > TIME_TO_VALID_TARGET)
            targetReached();
    }
    else {
        targetTimes = 0;
    }
}
void Feedback::trialInterrupted()
{
    updateDisplayTimer->stop();
}

void Feedback::stopBlock()
{
    updateDisplayTimer->stop();
}

int Feedback::BalltoTargetDist(float ballXCenter, float ballYCenter) {
   return (int) sqrt(pow(ballXCenter - ((float)realTargetXCenter),2) + pow(ballYCenter - ((float)realTargetYCenter),2));
}

void Feedback::targetReached() {
    iTargets[iTargetNum]->setBrush(QBrush(QColor(0,0,160)));
    iTargets[iTargetNum]->setPen(QPen(QColor(0,0,160)));
}

Feedback::~Feedback()
{

    cout << "destroying targets" << endl;
    for(int i = 0; i < TARGET_NUMBER; i++)
        if(iTargets[i]) delete(iTargets[i]);
    iTargets.clear();
    cout << "destroying ball" << endl;
    if(iBall)
        delete(iBall);


    cout << "destroying scene" << endl;
    if(iScene) {
        iScene->clear();
        delete(iScene);
    }
    cout << "destroying timer" << endl;
    if(updateDisplayTimer) delete(updateDisplayTimer);
}

bool Feedback::getFeedbackError()
{
    return fError;
}
