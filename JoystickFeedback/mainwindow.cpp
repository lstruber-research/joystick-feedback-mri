#include "mainwindow.h"
#include "ui_mainwindow.h"


using namespace std;


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{

    ui->setupUi(this);

    // Recover user folder
    char *user;
    size_t len;
    errno_t err = _dupenv_s(&user, &len, "USERPROFILE" );
    if(err || (!user))
    {
        QMessageBox::critical(this,tr("Powered by OptiMove inc."),tr("Impossible de trouver le dossier utilisateur"));
        exit(0);
    }

    // Get user name
    bool userDefined;
    fileNumberFam = 0;
    fileNumberRot = 0;
    fileNumberRdmRot = 0;

    do {
        bool okReturned;
        userName = QInputDialog::getText(this, tr("Powered by OptiMove inc."), tr("Acronyme sujet :"), QLineEdit::Normal, trUtf8("Numéro + Initiales + Jour"), &okReturned, Qt::WindowFlags());
        if (!okReturned)
            exit(0);

        if(userName.isEmpty())
            userDefined = false;
        else {
            QRegExp StringMatcher("^[0-9]{1,2}[A-Z]{2,3}[0-9]{1,1}$", Qt::CaseInsensitive);
            if (StringMatcher.exactMatch(userName))
            {
                // Check if username folder exists
                strcpy_s(dataFolder, user);
                strcat_s(dataFolder, "\\Documents\\Manips");
                if(!QDir(dataFolder).exists())
                    QDir().mkdir(dataFolder);
                strcat_s(dataFolder, "\\JoystickFeedback\\");
                if(!QDir(dataFolder).exists())
                    QDir().mkdir(dataFolder);
                strcat_s(dataFolder, "\\Manip_IRM_LS_2020\\");
                if(!QDir(dataFolder).exists())
                    QDir().mkdir(dataFolder);
                strcat_s(dataFolder, userName.toStdString().c_str());
                if(!QDir(dataFolder).exists())
                {
                    QDir().mkdir(dataFolder);
                    userDefined = true;
                }
                else
                {
                    // it already exist, ask if delete
                    QMessageBox existBox(QMessageBox::Question,tr("Powered by OptiMove inc."),trUtf8("Le sujet ") + userName + trUtf8(" semble déjà exister dans la base de données. Voulez-vous supprimer les données existantes ou continuer ?"), QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel);
                    existBox.setButtonText(QMessageBox::Yes, trUtf8("Supprimer"));
                    existBox.setButtonText(QMessageBox::No, trUtf8("Continuer"));
                    existBox.setButtonText(QMessageBox::Cancel, trUtf8("Annuler"));

                    int r = existBox.exec();
                    if(r == QMessageBox::Yes)
                    {
                        // it already exist, ask if delete
                        QMessageBox deletebox(QMessageBox::Question,tr("Powered by OptiMove inc."),trUtf8("Etes vous vraiment sûr de vouloir supprimer TOUTES les données du sujet ") + userName + trUtf8(" ?\nCette action est irréversible !"), QMessageBox::Yes | QMessageBox::No);
                        deletebox.setButtonText(QMessageBox::Yes, trUtf8("Oui"));
                        deletebox.setButtonText(QMessageBox::No, trUtf8("Non"));
                        int r = deletebox.exec();
                        if(r == QMessageBox::No)
                        {
                            userDefined = false;
                        }
                        else
                        {
                            QDir dir(dataFolder);
                            dir.setNameFilters(QStringList() << "*.*");
                            dir.setFilter(QDir::Files);
                            foreach(QString dirFile, dir.entryList())
                                dir.remove(dirFile);
                            userDefined = true;
                        }
                    }
                    else if(r == QMessageBox::No)
                    {
                        // check how many blocs are available
                        userDefined = true;
                        QDir dir(dataFolder);
                        dir.setNameFilters(QStringList() << "RAW_F_*.csv");
                        dir.setFilter(QDir::Files);
                        int nbFilesFam = dir.count();
                        for(int i = 0; i < nbFilesFam; i++)
                        {
                            QString fileName(QString("RAW_F_") + QString::number(i+1) + QString("_") + userName + QString(".csv"));
                            cout << fileName.toStdString().c_str() << endl;
                            QFileInfo checkFile(QString(dataFolder) + QString("\\") + fileName);
                            cout << checkFile.absoluteFilePath().toStdString().c_str() << endl;
                            if(!checkFile.exists() || !checkFile.isFile())
                            {
                                QMessageBox::critical(this,tr("Powered by OptiMove inc."),trUtf8("Erreur lors de la lecture des données existantes"));
                                userDefined = false;
                                break;
                            }
                        }
                        fileNumberFam = nbFilesFam;

                        // check how many blocs are available
                        userDefined = true;
                        dir.setNameFilters(QStringList() << "RAW_R_*.csv");
                        dir.setFilter(QDir::Files);
                        int nbFilesRot = dir.count();
                        for(int i = 0; i < nbFilesRot; i++)
                        {
                            QString fileName(QString("RAW_R_") + QString::number(i+1) + QString("_") + userName + QString(".csv"));
                            cout << fileName.toStdString().c_str() << endl;
                            QFileInfo checkFile(QString(dataFolder) + QString("\\") + fileName);
                            cout << checkFile.absoluteFilePath().toStdString().c_str() << endl;
                            if(!checkFile.exists() || !checkFile.isFile())
                            {
                                QMessageBox::critical(this,tr("Powered by OptiMove inc."),trUtf8("Erreur lors de la lecture des données existantes"));
                                userDefined = false;
                                break;
                            }
                        }
                        fileNumberRot = nbFilesRot;

                        // check how many blocs are available
                        userDefined = true;
                        dir.setNameFilters(QStringList() << "RAW_RR_*.csv");
                        dir.setFilter(QDir::Files);
                        int nbFilesRdmRot = dir.count();
                        for(int i = 0; i < nbFilesRdmRot; i++)
                        {
                            QString fileName(QString("RAW_RR_") + QString::number(i+1) + QString("_") + userName + QString(".csv"));
                            cout << fileName.toStdString().c_str() << endl;
                            QFileInfo checkFile(QString(dataFolder) + QString("\\") + fileName);
                            cout << checkFile.absoluteFilePath().toStdString().c_str() << endl;
                            if(!checkFile.exists() || !checkFile.isFile())
                            {
                                QMessageBox::critical(this,tr("Powered by OptiMove inc."),trUtf8("Erreur lors de la lecture des données existantes"));
                                userDefined = false;
                                break;
                            }
                        }
                        fileNumberRdmRot = nbFilesRdmRot;
                    }
                    else
                        userDefined = false;

                }
            }
            else {
                userDefined = false;
            }
        }
    } while(!userDefined);

    Logger("User defined, starting app", dataFolder);
    Logger("Reading constants file", dataFolder);

    // Read constants file
    map<string, int> gVar;
    bool constRead = true;

    ifstream constFile("constants.txt");
    if (constFile.is_open())
    {
        string VarName;
        while(constFile >> VarName)
        {
            if(!(constFile >> gVar[VarName]))
                constRead = false;
        }
        constFile.close();
    }
    else constRead = false;

    if(constRead){
        if(gVar.find("TARGET_NUMBER") == gVar.end()) constRead = false; else TARGET_NUMBER = gVar["TARGET_NUMBER"];
        if(gVar.find("TARGET_DISTANCE") == gVar.end()) constRead = false; else TARGET_DISTANCE = gVar["TARGET_DISTANCE"];
        if(gVar.find("TARGET_SIZE") == gVar.end()) constRead = false; else TARGET_SIZE = gVar["TARGET_SIZE"];
        if(gVar.find("CENTER_SIZE") == gVar.end()) constRead = false; else CENTER_SIZE = gVar["CENTER_SIZE"];
        if(gVar.find("BALL_SIZE") == gVar.end()) constRead = false; else BALL_SIZE = gVar["BALL_SIZE"];

        if(gVar.find("CONSTANT_DEV") == gVar.end()) constRead = false; else CONSTANT_DEV = gVar["CONSTANT_DEV"];
        if(gVar.find("RDM_MAX_DEV") == gVar.end()) constRead = false; else RDM_MAX_DEV = gVar["RDM_MAX_DEV"];
        if(gVar.find("RDM_STEP_DEV") == gVar.end()) constRead = false; else RDM_STEP_DEV = gVar["RDM_STEP_DEV"];

        if(gVar.find("REST_TR") == gVar.end()) constRead = false; else REST_TR = gVar["REST_TR"];
        if(gVar.find("TRIALS_TR") == gVar.end()) constRead = false; else TRIALS_TR = gVar["TRIALS_TR"];
        if(gVar.find("INIT_TR") == gVar.end()) constRead = false; else INIT_TR = gVar["INIT_TR"];
        if(gVar.find("NB_TRIALS") == gVar.end()) constRead = false; else NB_TRIALS = gVar["NB_TRIALS"];
        if(gVar.find("DISPLAY_TARGET_TIME") == gVar.end()) constRead = false; else DISPLAY_TARGET_TIME = gVar["DISPLAY_TARGET_TIME"];
        if(gVar.find("TIME_TO_VALID_TARGET") == gVar.end()) constRead = false; else TIME_TO_VALID_TARGET = gVar["TIME_TO_VALID_TARGET"];
        if(gVar.find("TR_IRM") == gVar.end()) constRead = false; else TR_IRM = gVar["TR_IRM"];
        if(gVar.find("RESTING_SCAN_TIME") == gVar.end()) constRead = false; else RESTING_SCAN_TIME = gVar["RESTING_SCAN_TIME"];

        if(gVar.find("DATA_RATE") == gVar.end()) constRead = false; else DATA_RATE = gVar["DATA_RATE"];

        if(gVar.find("NB_BLOCKS_NORMAL") == gVar.end()) constRead = false; else NB_BLOCKS_NORMAL = gVar["NB_BLOCKS_NORMAL"];
        if(gVar.find("NB_BLOCKS_ROT") == gVar.end()) constRead = false; else NB_BLOCKS_ROT = gVar["NB_BLOCKS_ROT"];
        if(gVar.find("NB_BLOCKS_RANDOM_ROT") == gVar.end()) constRead = false; else NB_BLOCKS_RANDOM_ROT = gVar["NB_BLOCKS_RANDOM_ROT"];

        if(gVar.find("BOARD_NUM") == gVar.end()) constRead = false; else BOARD_NUM = gVar["BOARD_NUM"];
        if(gVar.find("JOY_X_CHANNEL") == gVar.end()) constRead = false; else JOY_X_CHANNEL = gVar["JOY_X_CHANNEL"];
        if(gVar.find("JOY_Y_CHANNEL") == gVar.end()) constRead = false; else JOY_Y_CHANNEL = gVar["JOY_Y_CHANNEL"];
        if(gVar.find("IRM_TRIG_CHANNEL") == gVar.end()) constRead = false; else IRM_TRIG_CHANNEL = gVar["IRM_TRIG_CHANNEL"];

        // Compute possibles deviation angles from constants
        PossibleDevAngles.clear();
        int NbStepsDev = RDM_MAX_DEV/RDM_STEP_DEV;
        cout << "Possible deviation angles : ";
        for(int i = 0; i <= NbStepsDev; i++)
        {
            PossibleDevAngles.push_back(-RDM_MAX_DEV + i*RDM_STEP_DEV);
            cout << -RDM_MAX_DEV + i*RDM_STEP_DEV << ", ";
        }
        for(int i = NbStepsDev; i >= 0; i--)
        {
            PossibleDevAngles.push_back(RDM_MAX_DEV - i*RDM_STEP_DEV);
            cout << RDM_MAX_DEV - i*RDM_STEP_DEV << ", ";
        }
        cout << endl;

        // Compute Trial Time
        trialTime = (int)(((double)(TR_IRM*TRIALS_TR))/((double)NB_TRIALS));

        Logger("Computation of trial time from constants : " + to_string(static_cast<long long>(trialTime)) + " ms", dataFolder);

        // Create Target list
        TargetList.clear();
        for(int i = 0 ; i < TARGET_NUMBER; i++)
            TargetList.push_back(i);
    }

    if(!constRead)
    {
        Logger("Error while reading constants file, exiting...", dataFolder);
        QMessageBox::critical(this,tr("Powered by OptiMove inc."),tr("Impossible de lire les constantes"));
        exit(0);
    }

    // Initializing and configuring joystick
    int lowChan = min(min(JOY_X_CHANNEL,JOY_Y_CHANNEL),IRM_TRIG_CHANNEL);
    LOW_CHANNEL = lowChan;
    int HighChan = max(max(JOY_X_CHANNEL,JOY_Y_CHANNEL),IRM_TRIG_CHANNEL);
    HIGH_CHANNEL = HighChan;

    float revision = (float)CURRENTREVNUM;
    int ULStat = cbDeclareRevision(&revision);
    cbErrHandling(PRINTALL, DONTSTOP);
    if(ULStat != 0)
    {
        // Error
        Logger("Error while initializing joystick, exiting...", dataFolder);
        QMessageBox::critical(this,tr("Powered by OptiMove inc."),tr("Impossible d'initialiser le joystick"));
        // exit(0);
    }

    unsigned Options = NOCONVERTDATA + SINGLEIO;
    long Rate = (long) DATA_RATE;
    long Count = 0.25 * Rate * 2; // Seconds * Samples/sec * NbChannels
    WORD* BufferData = new WORD[Count];
    ULStat = cbAInScan (BOARD_NUM, JOY_X_CHANNEL, JOY_Y_CHANNEL, Count, &Rate, BIP5VOLTS, BufferData, Options);
    if(ULStat != 0)
    {
        // Error
        Logger("Error while initializing joystick, exiting...", dataFolder);
        QMessageBox::critical(this,tr("Powered by OptiMove inc."),tr("Impossible d'initialiser le joystick"));
        exit(0);
    }

    short Status = 0;
    long CurCount, CurIndex;
    ULStat = cbGetStatus(BOARD_NUM, &Status, &CurCount, &CurIndex, AIFUNCTION);
    if(ULStat != 0 || Status != 0)
    {
        // Error
        Logger("Error while initializing joystick, exiting...", dataFolder);
        QMessageBox::critical(this,tr("Powered by OptiMove inc."),tr("Impossible d'initialiser le joystick"));
        exit(0);
    }

    JoyXCalib = 0;
    JoyYCalib = 0;
    for(int i = 0; i < CurCount; i = i + 2)
    {
        JoyYCalib += ((float)BufferData[i]) * 10/65535 - 5;
        JoyXCalib += ((float)BufferData[i+1]) * 10/65535 - 5;
    }
    JoyYCalib /= CurCount/2;
    JoyXCalib /= CurCount/2;

    JoyXAmp = 1.2f;
    JoyYAmp = 1.0f;

    JoyXCalib = 2.36f;
    JoyYCalib = 2.11f;

    Logger("Joystick initialized. Calibration : " + to_string(static_cast<long double>(JoyXCalib)) + "V, " + to_string(static_cast<long double>(JoyYCalib)) + "V", dataFolder);
    cout << "Joystick calib : " << JoyXCalib << " ; " << JoyYCalib << endl;
    cout << "Joystick amp : " << JoyXAmp << " ; " << JoyYAmp << endl;

    trialNumber = 0;

    ui->BackButton->setVisible(false);
    ui->BackButton->setEnabled(false);
    switchScreen(0);
    iPhase = 0;
    iTrigRadius = 0;
}

MainWindow::~MainWindow()
{
    //feedback.wait();
    delete ui;
}

void MainWindow::on_StartButton_clicked()
{
    switchScreen(1);
    int nbBlocks = 0;
    int fileNumber = 0;
    switch(iPhase)
    {
        case NORMAL :
            Logger("Starting familiarization", dataFolder);
            fileNumber = fileNumberFam;
            nbBlocks = NB_BLOCKS_NORMAL;
            break;
        case ROT :
            Logger("Starting adaptation", dataFolder);
            fileNumber = fileNumberRot;
            nbBlocks = NB_BLOCKS_ROT;
            break;
        case RANDOM_ROT :
            Logger("Starting random rotation", dataFolder);
            fileNumber = fileNumberRdmRot;
            nbBlocks = NB_BLOCKS_RANDOM_ROT;
            break;
    }

    dispFeedback = new Feedback(*(ui->FeedbackView), iPhase, iTrigRadius);
    if(dispFeedback->isInitCanceled())
    {
        if(dispFeedback) delete(dispFeedback);
        return;
    }
    if(dispFeedback->getFeedbackError()) {
        QMessageBox::critical(this,tr("Powered by OptiMove inc."),tr("Impossible de dialoguer avec la carte MC"));
        Logger("Error while conversing with MC acquisition card", dataFolder);
        if(dispFeedback) delete(dispFeedback);
        return;
    }

    EnableButtons(0);

    threadFeedback = new FeedbackThread(iPhase, nbBlocks, dataFolder, userName, fileNumber);
    connect(threadFeedback, SIGNAL(doDisplayCross()), dispFeedback, SLOT(displayCross()));
    connect(threadFeedback, SIGNAL(newTrial(int, double)), dispFeedback, SLOT(startNewTrial(int, double)));
    connect(threadFeedback, SIGNAL(startToCenter()), dispFeedback, SLOT(initToCenter()));
    connect(threadFeedback, SIGNAL(backToCenter()), dispFeedback, SLOT(setTargetToCenter()));
    connect(threadFeedback, SIGNAL(endBlock()), dispFeedback, SLOT(stopBlock()));
    connect(threadFeedback, SIGNAL(endTrial()), this, SLOT(trialFinished()));
    connect(threadFeedback, SIGNAL(interruptTrial()), dispFeedback, SLOT(trialInterrupted()));
    connect(dispFeedback, SIGNAL(feedbackError()), threadFeedback, SLOT(onError()));
    connect(threadFeedback, SIGNAL(fbError()), this, SLOT(onError()));
    connect(threadFeedback, SIGNAL(finished()), this, SLOT(onFinished()));
    connect(threadFeedback, SIGNAL(crossScreen()), this, SLOT(onCrossScreen()));

    connect(dispFeedback, SIGNAL(dataRequest()), threadFeedback, SLOT(onDataRequest()));
    connect(threadFeedback, SIGNAL(dataSent(WORD,WORD)), dispFeedback, SLOT(onDataReceived(WORD,WORD)));

    threadFeedback->start(); 
}

void MainWindow::switchScreen(int screen)
{
    // Get screen size
    QRect rec0 = QApplication::desktop()->screenGeometry(0);
    if((screen == 1) && (QApplication::desktop()->screenCount() > 1))
    {
        QRect rec = QApplication::desktop()->screenGeometry(screen);
        screenHeight = rec.height();
        screenWidth = rec.width();

        int dpiX = qApp->desktop()->logicalDpiX();
        FLOAT scaleFactor = dpiX / 96.0f; // this is same as devicePixelRatio

        cout << "screen 0 : " << rec0.width() << "x" << rec0.height() << ";" << scaleFactor << endl;
        cout << "screen 1 : " << rec.width() << "x" << rec.height() << endl;

        this->setGeometry(rec0.width()*scaleFactor,0,screenWidth,screenHeight);
    }
    else
    {
        screenHeight = rec0.height();
        screenWidth = rec0.width();
        this->setGeometry(0,0,screenWidth,screenHeight);
    }

    // View
    ui->FeedbackView->resize(screenWidth, screenHeight);
    ui->FeedbackView->setSceneRect(-screenWidth/2,-(screenHeight)/2,screenWidth,screenHeight);
    ui->FeedbackView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->FeedbackView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    int startButtonWidth = ui->StartButton->width();
    int startButtonHeight = ui->StartButton->height();
    ui->StartButton->move((screenWidth - startButtonWidth)/2, (screenHeight-startButtonHeight)/2);

    int closeButtonWidth = ui->CloseButton->width();
    int closeButtonHeight = ui->CloseButton->height();
    ui->CloseButton->move(screenWidth - closeButtonWidth - 25, closeButtonHeight - 5);

    int backButtonWidth = ui->BackButton->width();
    int backButtonHeight = ui->BackButton->height();
    ui->BackButton->move(screenWidth - backButtonWidth - 25, backButtonHeight - 5);

    int comboWidth = ui->comboBox->width();
    int comboHeight = ui->comboBox->height();
    ui->comboBox->move((screenWidth - comboWidth)/2, (screenHeight-comboHeight)/2 + startButtonHeight/2 + 50);
    QGraphicsScene* scene = new QGraphicsScene();
    scene->setBackgroundBrush(QColor(255,255,255));
    ui->FeedbackView->setScene(scene);

    this->showFullScreen();
}

void MainWindow::trialFinished()
{
    trialNumber++;
}

void MainWindow::onCrossScreen() {
    QGraphicsScene* scene = new QGraphicsScene();
    QImage imageBackground(":/img/cross");
    QGraphicsPixmapItem* background = new QGraphicsPixmapItem(QPixmap::fromImage(imageBackground));

    background->scale(((double)screenWidth)/((double)imageBackground.width()),((double)screenHeight)/((double)imageBackground.height()));
    background->setPos(-screenWidth/2,-screenHeight/2);
    scene->addItem(background);
    ui->FeedbackView->setScene(scene);
}

void MainWindow::onFinished()
{
    if(dispFeedback) delete(dispFeedback);
    EnableButtons(1);
    switchScreen(0);
    trialNumber = 0;

    // Selection auto de la phase suivante
    switch(iPhase)
    {
        case NORMAL :
            fileNumberFam++;
            break;
        case ROT :
            fileNumberRot++;
            break;
        case RANDOM_ROT :
            fileNumberRdmRot++;
            break;
    }

    ui->comboBox->setCurrentIndex((iPhase+1)%4);
}

void MainWindow::on_CloseButton_clicked()
{
    this->close();
}

void MainWindow::EnableButtons(bool enabled)
{
    ui->StartButton->setEnabled(enabled);
    ui->StartButton->setVisible(enabled);
    ui->CloseButton->setEnabled(enabled);
    ui->CloseButton->setVisible(enabled);
    ui->comboBox->setEnabled(enabled);
    ui->comboBox->setVisible(enabled);

    if(iPhase != 0) {
        ui->BackButton->setVisible(!enabled);
        ui->BackButton->setEnabled(!enabled);
    }
}

void MainWindow::on_BackButton_clicked()
{
    threadFeedback->interrupt();
}

void MainWindow::on_comboBox_currentIndexChanged(int index)
{
    iPhase = index;
}

void MainWindow::onError()
{
    QMessageBox::critical(this,tr("Powered by OptiMove inc."),tr("Impossible de dialoguer avec la carte MC"));
}
