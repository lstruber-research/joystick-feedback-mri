#ifndef FEEDBACKTHREAD_H
#define FEEDBACKTHREAD_H

#include <iostream>
#include <QThread>
#include <QTimer>
#include <QEventLoop>
#include <QGraphicsView>
#include "feedback.h"
#include "savingthread.h"
#include <math.h>

class FeedbackThread : public QThread
{
    Q_OBJECT
public:
    FeedbackThread(int phase, int nbBlocks, char* dataFolder, QString username, int fileNumber);
    ~FeedbackThread();
    void interrupt();

signals:
    void newTrial(int target, double deviation);
    void startToCenter();
    void backToCenter();
    void doDisplayCross();
    void endTrial();
    void endBlock();
    void interruptTrial();
    // void intermediateEvent(double time);
    void fbError();
    void crossScreen();
    void dataSent(WORD JoyX, WORD JoyY);

public slots:
    void onError();
    void onDataRequest();
    void saveFinished();

private:
    void run();
    void extractAngleSequence();
    bool launchRecording(long Count, long Rate, WORD* BufferData);
    void setTrial(int value, long idx);
    void setKinematicTrigger(int value, long idx);
    void setTargetNumber(int value, long idx);
    void setAngle(int value, long idx);
    long getCurrentIdx();
    bool stopRecording(int numBlock);
    bool waitIRMTrig();

    char mDataFolder[100];
    bool targetReached;
    bool feedbackError;
    vector<double> DevAngleVector;
    vector<double> TargetsVector;
    int iNbBlocks, iPhase;
    bool stopRequest;
    char fileName[150];
    long Rate, Count;
    int iNbChannels;
    WORD*  BufferData;
    int* trialNumber;
    int* kinTrigValue;
    int* targetNumber;
    int* angleValue;

    SavingThread *threadSave;

};

#endif // FEEDBACKTHREAD_H
