#ifndef SAVINGTHREAD_H
#define SAVINGTHREAD_H

#include <iostream>
#include <QThread>
#include <QTimer>
#include <QEventLoop>
#include <QGraphicsView>
#include "feedback.h"

class SavingThread : public QThread
{
public:
    SavingThread(char* fileName, int numBlock, int Count, int NbChannels, WORD* BufferData, int* trialNumber, int* kinTrigValue, int* targetNumber, int* angleValue);
    ~SavingThread();
    bool getSaveState();

private:
    void run();

    char sfileFullName[150];
    int iNumBlock, iCount, iNbChannels;
    WORD* iBufferData;
    int* iTrialNumber;
    int* iKinTrigValue;
    int* iTargetNumber;
    int* iAngleValue;
    bool SaveOK;
};

#endif // SAVINGTHREAD_H
