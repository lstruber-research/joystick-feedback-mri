#include "savingthread.h"

using namespace std;

SavingThread::SavingThread(char* fileName, int numBlock, int Count, int NbChannels, WORD* BufferData, int* trialNumber, int* kinTrigValue, int* targetNumber, int* angleValue)
{
    // SAVING ALL DATA
    strcpy_s(sfileFullName, fileName);
    char blockNumberChar[4] ;
    sprintf_s(blockNumberChar, "%d", numBlock + 1);

    strcat_s(sfileFullName, "_B");
    strcat_s(sfileFullName, blockNumberChar);
    strcat_s(sfileFullName, ".csv");

    iCount = Count;
    iNbChannels = NbChannels;
    iBufferData = new WORD[Count];
    memcpy(iBufferData,BufferData,iCount*sizeof(WORD));
    iTrialNumber = new int[iCount/iNbChannels];
    memcpy(iTrialNumber,trialNumber,iCount/iNbChannels*sizeof(int));
    iKinTrigValue = new int[iCount/iNbChannels];
    memcpy(iKinTrigValue,kinTrigValue,iCount/iNbChannels*sizeof(int));
    iTargetNumber = new int[iCount/iNbChannels];
    memcpy(iTargetNumber,targetNumber,iCount/iNbChannels*sizeof(int));
    iAngleValue = new int[iCount/iNbChannels];
    memcpy(iAngleValue,angleValue,iCount/iNbChannels*sizeof(int));

    SaveOK = true;
}

void SavingThread::run(){
    cout << "saving : " << sfileFullName << endl;
    clock_t start_saving = clock();
    ofstream dataFile;
    dataFile.open(sfileFullName, fstream::out | fstream::app);
    dataFile.clear();
    if(!dataFile) {
        cout << "Error opening datafile" << endl;
        SaveOK = false;
        return;
    }
    dataFile << "Trial number; Kinematic trigger; IRM Trigger; Target number; Angle; Joy_X; Joy_Y;\n";
    for(int i = 0; i < iCount; i = i + iNbChannels) {
        float XVolt = ((float)iBufferData[i+1]) * 10/65535 - 5;
        float YVolt = ((float)iBufferData[i]) * 10/65535 - 5;
        float IRMTrigVolt = ((float)iBufferData[i + 2]) * 10/65535 - 5;
        float JoyX = -((((float)TARGET_DISTANCE) + 100)/JoyXAmp * (XVolt - JoyXCalib));
        float JoyY = -((((float)TARGET_DISTANCE) + 100)/JoyYAmp * (YVolt - JoyYCalib));
        dataFile << iTrialNumber[i/iNbChannels] << " ;";
        dataFile << iKinTrigValue[i/iNbChannels] << " ;";
        dataFile << IRMTrigVolt << " ;";;
        dataFile << iTargetNumber[i/iNbChannels] << " ;";
        dataFile << iAngleValue[i/iNbChannels] << " ;";
        dataFile << JoyX << " ;";
        dataFile << JoyY << " ;\n";
    }
    dataFile.close();
    clock_t end_saving = clock();
    double save_time = (double) (end_saving - start_saving)/CLOCKS_PER_SEC * 1000;
    cout << "saving time : " << save_time << endl;
    SaveOK = true;
}

bool SavingThread::getSaveState() {
    return SaveOK;
}

SavingThread::~SavingThread()
{
    if(iBufferData) delete[] iBufferData;
    if(iTrialNumber) delete[] iTrialNumber;
    if(iKinTrigValue) delete[] iKinTrigValue;
    if(iTargetNumber) delete[] iTargetNumber;
    if(iAngleValue) delete[] iAngleValue;
}
