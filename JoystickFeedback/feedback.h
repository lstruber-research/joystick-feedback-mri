#ifndef FEEDBACK_H
#define FEEDBACK_H

#include <QObject>
#include <QGraphicsScene>
#include <QGraphicsItem>
#include <QDebug>
#include <QEvent>
#include <QKeyEvent>
#include <QGraphicsView>
#include "constants.h"
#include "time.h"
#include <QTimer>
#include <QDir>
#include <QMessageBox>
#include <QString>
#include <iostream>
#include <fstream>
#include "cbw.h"

#define PI   3.14159265358979323846

class QGraphicsItem;

using namespace std;
class Feedback : public QObject
{
    Q_OBJECT
public:
    explicit Feedback(QGraphicsView &view, int phase, int trigRadius);
    ~Feedback();
    int BalltoTargetDist(float ballX, float ballY);
    bool getFeedbackError();
    bool isInitCanceled();
    void startEvent();
    void targetReached();

public slots :
    void startNewTrial(int TargetNum, double Angle);
    void initToCenter();
    void setTargetToCenter();
    void displayCross();
    void setBallPos(float x, float y);
    void dataRequestForDisplayUpdate();
    void stopBlock();
    void trialInterrupted();
    void onDataReceived(WORD JoyX, WORD JoyY);

signals:
    void feedbackError();
    void dataRequest();

private:
    QGraphicsView &iView;
    QGraphicsScene *iScene;
    QGraphicsEllipseItem *iBall;
    QGraphicsEllipseItem *iCenter;
    QGraphicsLineItem *iCrossVert;
    QGraphicsLineItem *iCrossHor;
    vector<QGraphicsEllipseItem*> iTargets;
    int iTargetNum;
    double dAngle;
    double dStartAngularDev;
    double dRealAngularDev;
    int startTargetXCenter, startTargetYCenter;
    int realTargetXCenter, realTargetYCenter;
    QTimer* updateDisplayTimer;
    int targetTimes;
    ofstream dataFile;
    int iPhase;
    bool fError;
    bool initCanceled;
    float JoystickX_old, JoystickY_old, BallXCenter_old, BallYCenter_old;
    bool eventLaunched;
    int iTrigRadius;
    bool bTargetToCenter;
};

#endif // FEEDBACK_H
